# MuColPrinciples


## Description

This repository contains python notebooks which work through the basics principles of muon colliders

The original questions can be found [here](https://docs.google.com/document/d/1DYr0utChmdgr3WKub41Gc3CzhdsvZpTbJVY-dMRTYhs/edit?usp=sharing)

Solutions to the questions can be found in the corresponding documents
* 1-accelerators.ipynb
* 2-lifetime.ipynb
* 3-detector.ipynb

## Getting started

* If you aren't sure how to use python, or gitlab start [here](https://docs.google.com/document/d/1GhYcyMfPS0qoDGMFuR1lazSPDd0ZJatQSkY48k1aMAY/edit#)
* If you've never used a python notebook start [here](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)


